package oop;

public class ObjectClassMethods {

    static class A {
        int value ;

        @Override
        public String toString() {
            return "A{" +
                    "value=" + value +
                    '}';
        }

        @Override
        public boolean equals(Object obj) {
            if(obj instanceof A)
            {
                A a = (A) obj;
                return value ==  a.value;
            }
            return false ;

        }
        @Override
        public int hashCode() {
            return super.hashCode();
        }

        @Override
        public A clone() throws CloneNotSupportedException {
            A t = new A();
            t.value = value ;
            return t;
        }
    }

    static class B extends A{
        int value ;

        @Override
        public String toString() {
            return "A{" +
                    "value of A=" + super.value + ", value of B="+value+'}';
        }

        public void setAValue(int i) {
            super.value = i ;
        }


    }

    public static void main(String[] args) throws CloneNotSupportedException {

        A a = new A();
        a.value = 50 ;

        System.out.println("a = " + a);                     // will call a.toString() implicitly
        System.out.println("a = " + a.toString());

        //String s = a ;                              // a.toString() will no call a.toString() implicitly


        B b = new B();

        b.value = 20 ;

        b.setAValue(10);

        System.out.println("b = " + b);


        A a1 = new A();
        a1.value = 5 ;
        A a2 = new A();
        a2.value = 5 ;
       // a2 = a1 ;
        System.out.println("(a1 == a2) = " + (a1 == a2));
        System.out.println("a1.equals(a2) = " +a1.equals(a2) );

        System.out.println("a1.equals(a2) = " +a1.equals("") );

        A a3 = a1.clone() ;

        System.out.println("a1 = " + a1);
        System.out.println("a3 = " + a3);

    }
}
