package oop;

public class ThisAndSuper {
    // this : reference from inside the object to itself
    // this: also used to call a constructor from another constructor in the same object

    // super :reference from inside the object to its' parent object
    // super: also used to call a constructor from another constructor in the child object

    // can't use this or super from static contexts
    public static void main(String[] args) {


    }

    public void test()
    {
        this.equals(this);
        this.toString();

        super.toString() ;

    }
}
