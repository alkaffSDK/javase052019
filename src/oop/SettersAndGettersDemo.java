package oop;

public class SettersAndGettersDemo {

    static class Account {
        private String name ;
        private double value ;
        private int id ;
        private String password ;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

       public boolean checkPassword(String pass) {
            return password.equals(pass);
        }

        public boolean changePassword(String pass) {
            if(pass != null && pass.length() >= 8)
               {
                   this.password = password;
                   return  true ;
               }
            return  false ;
        }
    }
    public static void main(String[] args) {


        Account a = new Account();
        a.id =1;
        a.name = "Ahmed";
        a.value = -200.5;
        a.password = "123";
    }
}
