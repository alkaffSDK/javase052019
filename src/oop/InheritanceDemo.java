package oop;


import sharedClasses.Parent;
import sharedClasses.Point;

public class InheritanceDemo {


    // In java : only single inheritance is allowed !!
    // There is only one and only one parent class for any class in Java (except for Object class)
    static class PointChild extends Point  {
        // static member are no inheritable

        int x , y  ;

        public PointChild() {
            super(0,0,0);
        }
        public PointChild(int x) {
            super(x);
        }

        public PointChild(int x, int y) {
            super(x, y);
        }

        public PointChild(int x, int y, int z) {
            super(x, y, z);
        }

        public  void method()
        {
            x = 10 ;  // ir  this.x              // this x is the local member in this class
            super.x = 10 ;          // this x of the parent object (of type Point)
            z = 10 ;
            Protected = 10 ;
        }
        public static void staticMethod() {
            // Error  Protected = 10; //


        }

        // non static members are inheritable but not all of them are accessible  ( private and ( package for class in different packages ) are not )
        protected void nonStatic() {
            Public = 10;
            Protected = 10;
        }
    }

    static class Child extends PointChild {


    }

    public static void main(String[] args) {

        PointChild p = new PointChild();
        p.x = 10 ;
        Child c = new Child();
        Object d ;

    }
}
