package oop;

import sharedClasses.Point;

public class DataAbstraction {

    public static void main(String[] args) {

        int x , y,  z ;
        int x1 , y1,  z1 ;

        // we can replace that by defining Point class and
        Point p = new Point();
        p.x = 10 ;
        p.y = 2 ;
        p.z = 1 ;

        Point p1 = new Point();
        p1.x = 5 ;
        p1.y = 2 ;
        p1.z = 6 ;

    }
}
