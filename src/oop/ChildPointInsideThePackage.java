package oop;

import sharedClasses.Point;

public class ChildPointInsideThePackage extends Point {

    public void method() {
        Point p = new Point();
        // System.out.println("Private = " + p.Private);   // not accessible
        //System.out.println("Package = " + p.Package);     // not accessible
        //System.out.println("Protected = " + p.Protected);  // not accessible
        System.out.println("Public = " + p.Public);

        // this is the protected variable from the parent (super) object
        Protected = 10 ;
    }

    public static void main(String[] args) {
        Point p = new Point();



    }
}
