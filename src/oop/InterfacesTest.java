package oop;

public class InterfacesTest {

     interface  A {
        void a();
    }
    interface  B  extends  A{
        void b();
    }

    interface C extends  B {

        default void defaultMethod()
        {
            // a default method in the interface is a method that have a body must be be implemented by any sub class
            // must not override it, but you can
        }

        static void staticMethod()
        {
            // a static method in the interface is a method that have a body, but it has NOTHING to do with inheritance and implementation
            // called interfaceName.staticMethod() ;
            // can't override it

        }

        private void privateMethod(){
            // stating from Java 9: a private method in the interface is a method that have a body and can be used only inside the interface itself
            // can't override it
        }
    }

    class TestClass implements C {

        @Override
        public void a() {

        }

        @Override
        public void b() {

        }

    }


    public static void main(String[] args) {

    }
}
