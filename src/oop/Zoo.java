package oop;

import java.util.Locale;
import java.util.Random;

public class Zoo {

    static  class Animal {

        @Override
        public String toString() {
            return "Animal{}";
        }
    }

    static  class Bird extends  Animal {
        @Override
        public String toString() {
            return super.toString() + "->"+ "Bird{}";
        }
    }

    static  class Mammal extends  Animal  {
        @Override
        public String toString() {
            return super.toString() + "->"+"Mammal{}";
        }
    }

    static  class Penguin extends  Bird {
        @Override
        public String toString() {
            return super.toString() + "->"+"Penguin{}";
        }
    }

    static  class Falcon extends  Bird implements Flyable{
        @Override
        public String toString() {
            return super.toString() + "->"+"Falcon{}";
        }

        @Override
        public String canFly() {
            return  ", it can fly.";
        }

    }

    static  class Cat extends  Mammal {
        @Override
        public String toString() {
            return super.toString() + "->"+"Cat{}";
        }
    }

    static  class Bat extends  Mammal implements Flyable{
        @Override
        public String toString() {
            return super.toString() + "->"+"Bat{}";
        }
        @Override
        public  String canFly()
        {
            return  ", it can fly.";
        }
    }

    public static void main(String[] args) {

        Random random = new Random();
        Animal[] animals  = new Animal[20];


        for (int i = 0; i < animals.length; i++) {
            switch (random.nextInt(7))
            {
                case 0:
                    animals[i] = new Penguin() ; break;
                case 1:
                    animals[i] = new Bat() ; break;
                case 2:
                    animals[i] = new Cat() ; break;
                case 3:
                    animals[i] = new Falcon() ; break;
                case 4:
                    animals[i] = new Mammal() ; break;
                case 5:
                    animals[i] = new Bird() ; break;
                default:
                    animals[i] = new Animal() ; break;
            }


            System.out.printf(Locale.getDefault(),"%5d) %s",i+1 , animals[i]);

//            if(animals[i] instanceof Falcon)
//            System.out.println(((Falcon)animals[i]).canFly());
//            else if (animals[i] instanceof  Bat)
//                System.out.println(((Bat)animals[i]).canFly());
//            else
//                System.out.println();

            if (animals[i] instanceof  Flyable)
                System.out.println(((Flyable)animals[i]).canFly());
            else
                System.out.println();
        }


    }

    static final int X = 0 ;

    interface Flyable {
        // by default: ALL variables in interface are public static final
        int X = 10 ;

        // by default:  methods in interface are public  abstract
          String canFly() ;
    }


}
