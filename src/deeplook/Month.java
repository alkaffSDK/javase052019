package deeplook;
public enum  Month {
    January (1,31,"Jan", "Sivan"),
    February (2,29,"Feb", "Tammuz"),
    March (3,31,"Mar", "Av");


    private int number , days ;
    private String Abbreviation, Alternative ;
     Month(int n, int days, String abriv, String alternative) {
        number = n ;
        this.days = days ;
        Abbreviation =abriv ;
        Alternative = alternative ;

    }

    public int getNumber() {
        return number;
    }

    public int getDays() {
        return days;
    }

    public String getAbbreviation() {
        return Abbreviation;
    }

    public String getAlternative() {
        return Alternative;
    }



//        - 31 days
//        February - 28 days in a common year and 29 days in leap years
//        March - 31 days
//        April - 30 days
//        May - 31 days
//        June - 30 days
//        July - 31 days
//        August - 31 days
//        September - 30 days
//        October - 31 days
//        November - 30 days
//        December - 31 days
//
//
//        Sivan, 30 days סיון
//        Tammuz, 29 days תמוז
//        Av, 30 days אב
//        Elul, 29 days אלול
//        Tishri, 30 days תשרי
//        Marcheshvan, 29/30 days מַרְחֶשְׁוָן
//        Kislev, 30/29 days כסלו
//        Tevet, 29 days טבת
//        Shevat, 30 days שבט
//        Adar 1, 30 days, intercalary month אדר א
//        Adar 2, 29 days אדר ב
//        Nisan, 30 days ניסן
//        Iyar, 30 days אייר
}