package deeplook;

public class InnerClassDemo {

    public static void main(String[] args) {
        Outer.StaticInner var1 = new Outer.StaticInner();


        Outer outerObject = new Outer();
        Outer.NonStaticInner var2 = outerObject.new NonStaticInner();
        // OR

        Outer.NonStaticInner var3 = new Outer().new NonStaticInner();
    }
}
