package deeplook;

public class StaticTester {
    public static void main(String[] args) {


        StaticDeepLook var ;
        System.out.println("-----------------");
        StaticDeepLook.staticVariable = 10 ;
        System.out.println("-----------------");

        var = new StaticDeepLook();
        System.out.println("-----------------");
        new StaticDeepLook();


    }
}
