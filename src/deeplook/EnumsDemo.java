package deeplook;

import javax.swing.*;
import java.lang.reflect.Field;

public class EnumsDemo {

    enum Gender {
        Male, Female
    }

    static class GenderClass {
        public static GenderClass Male = new GenderClass();
        public static GenderClass Female = new GenderClass();


        public String name() {
            for (Field f : this.getClass().getFields()) {
                try {
                    if (f.get(this).equals(this))
                        return f.getName();

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }
    }

    enum Day {
        Sun, Mon
    }

    static public class Human {
        String Name;
        int Age;
        Gender Gender;

        public Human(String name) {
            Name = name;
            Gender = EnumsDemo.Gender.Female;
        }


        public static void main(String[] args) {

            System.out.println("EnumsDemo.Gender.Female = " + EnumsDemo.Gender.Female);
            System.out.println("EnumsDemo.Gender.Female.name() = " + EnumsDemo.Gender.Female.name());
            System.out.println("EnumsDemo.Gender.Female.ordinal() = " + EnumsDemo.Gender.Female.ordinal());


            System.out.println("EnumsDemo.Gender.Male = " + EnumsDemo.Gender.Male);
            System.out.println("EnumsDemo.Gender.Male.name() = " + EnumsDemo.Gender.Male.name());
            System.out.println("EnumsDemo.Gender.Male.ordinal() = " + EnumsDemo.Gender.Male.ordinal());

            System.out.println("args = " + args);
            Month m = Month.February;
            System.out.println("m.name() = " + m.name());

            System.out.println(GenderClass.Female.name());
            System.out.println(GenderClass.Male.name());
        }
    }
}
