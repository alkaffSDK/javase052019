package regexpression;

import java.io.*;
import java.util.ArrayList;
import java.util.Locale;

public class TaskSplliter {

    public static void main(String[] args) {

        System.out.println("");
        String line = "";
        String[] splitted;
        PrintStream outputStream = null;
        try (BufferedReader reader = new BufferedReader(new FileReader(new File("test.txt")))) {

            line = reader.readLine();
            int[] lengths = new int[7];
            ArrayList<String[]> splitedLines = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                splitted = line.replaceAll("\"", "").split("(,(?=[\\d\\S]))");
                splitedLines.add(splitted);
                if (splitted.length == 7)
                    outputStream = System.out;
                else
                    outputStream = System.err;

                for (int i = 0; i < lengths.length; i++) {
                    if (splitted[i].length() > lengths[i])
                        lengths[i] = splitted[i].length();
                }
            }
            String format = String.format(Locale.getDefault(), "%%%ds | %%-%ds | %%-%ds | %%-%ds | %%-%ds | %%-%ds | %%-%ds%%n", lengths[0], lengths[1], lengths[2], lengths[3], lengths[4], lengths[5], lengths[6]);
            for (String[] l : splitedLines) {
                outputStream.printf(Locale.getDefault(), format, l[0], l[1], l[2], l[3], l[4], l[5], l[6]);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
