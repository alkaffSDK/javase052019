import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;

public class Test {
    public static void main(String[] args) {


        try(PrintWriter writer = new PrintWriter(new FileWriter(new File("../write.txt"))))
        {
            writer.println("hello");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
