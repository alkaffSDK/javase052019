package threading;

public class WhatIsAThread {

    Integer value = 0 ;

    Boolean ValueLock = false ;

    public  void increase(int counter) throws InterruptedException {
        for(int i=0;i<counter;i++) {

            Thread.sleep(10);
            System.out.println(Thread.currentThread().getName()+" --> increase :"+i);
                synchronized (ValueLock) {
                value++;
            }
        }
        System.out.println(Thread.currentThread().getName()+" --> The value of counter after increase is :"+value);
    }

    public  void decrease(int counter) throws InterruptedException {
        for(int i=0;i<counter;i++) {
           Thread.sleep(10);
            System.out.println(Thread.currentThread().getName()+" --> decrease :"+i);
            synchronized (ValueLock) {
                value--;
            }

        }
        System.out.println(Thread.currentThread().getName()+ " --> The value of counter after decrease is :"+value);
    }

    public static void main(String[] args) {

        WhatIsAThread whatIsAThread = new WhatIsAThread();
        long startTime = System.nanoTime() ;
        int counter = 100;
        System.out.println(Thread.currentThread().getName()+" --> The value of counter at the start is :"+whatIsAThread.value);
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    whatIsAThread.increase(counter);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    whatIsAThread.decrease(counter);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t2.start();
        t1.start();

        try {
            t1.join(5000);          // wait for the tread to finish with in 5000 ms
            t2.join();          // wait to finish
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName()+" --> The final value is "+ whatIsAThread.value);

        System.out.println(Thread.currentThread().getName()+" --> The time is "+ (System.nanoTime() - startTime));
    }
}
