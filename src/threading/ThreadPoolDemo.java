package threading;

import java.util.concurrent.*;

public class ThreadPoolDemo {


    public static void main(String[] args) {


        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue(10,true);
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(2,8,1, TimeUnit.MINUTES,workQueue) ;
        if(! poolExecutor.isShutdown())
        {
            workQueue.add(new Runnable() {
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName() + "workQueue");
                }
            });
        }

        poolExecutor.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + "Execute1");
            }
        });

        poolExecutor.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + "Execute2");
            }
        });

        poolExecutor.shutdown();

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);

        ThreadPoolExecutor poolExecutor1 = (ThreadPoolExecutor) Executors.newScheduledThreadPool(4);

       executor.submit(new Runnable() {
           @Override
           public void run() {

           }
       });


       Callable<Integer> callable = new Callable<Integer>() {
           @Override
           public Integer call() throws Exception {
               return null;
           }
       } ;
       Future<Integer> future =  executor.submit(callable ) ;

        try {
            // wait for the executor to execute the caller and return the result to the future
            System.out.println("future = " + future.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }


}
