package FileIO;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class ReadingTextFiles {

    public static void main(String[] args) {


        String filePath = "test.txt";

        System.out.println("=============readingUsingScanner=============");
        readingUsingScanner(filePath);

        System.out.println("=============readingFileInputStream=============");
        readingFileInputStream(filePath);

        System.out.println("=============readingFileInputStream1=============");
        readingFileInputStream1(filePath);

        System.out.println("=============readingBufferReader=============");
        readingBufferReader(filePath);

        System.out.println("=============readingWithFileAPI=============");
        readingWithFileAPI(filePath);

    }

    public static void readingUsingScanner(String filePath)
    {
        try (Scanner scanner = new Scanner(new File(filePath), Charset.forName("UTF-8")))
        {
            while(scanner.hasNextLine())
                System.out.println(scanner.nextLine());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void readingFileInputStream(String filePath)
    {
        try (FileInputStream fis   = new FileInputStream(new File(filePath)))
        {

            byte[] bytes ;
            int size = 0 ;

            if (( size = fis.available()) > 0 )
            {
                bytes = fis.readAllBytes() ;
                System.out.print(new String(bytes));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void readingFileInputStream1(String filePath)
    {
        try (FileInputStream fis   = new FileInputStream(new File(filePath)))
        {
            int len = 1024 ;
            byte[] buffer = new byte[len];
            int size = 0 ;

            while((size = fis.readNBytes(buffer,0,len)) > 0)
            {
                System.out.print(new String(buffer,0,size));
            }

            System.out.println("\nsize = " + size);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void readingBufferReader(String filePath)
    {
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(filePath))) )
        {
           String line = "";
           while ((line = reader.readLine()) != null)
           {
               System.out.println(line);
           }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }public static void readingWithFileAPI(String filePath)
    {
        try {
            Files.lines(Paths.get(filePath)).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
