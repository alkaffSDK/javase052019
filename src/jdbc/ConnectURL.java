package jdbc;/*=====================================================================
File: 	 ConnectURL.java
Summary: This Microsoft JDBC Driver for SQL Server sample application
         demonstrates how to connect to a SQL Server database by using 
		 a connection URL. It also demonstrates how to retrieve data 
		 from a SQL Server database by using an SQL statement.
---------------------------------------------------------------------
This file is part of the Microsoft JDBC Driver for SQL Server Code Samples.
Copyright (C) Microsoft Corporation.  All rights reserved.
 
This source code is intended only as a supplement to Microsoft
Development Tools and/or on-line documentation.  See these other
materials for detailed information regarding Microsoft code samples.
 
THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF
ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.
=====================================================================*/

import java.sql.*;
import java.util.Scanner;

public class ConnectURL {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        // Create a variable for the connection string.
        String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=JavaSE;integratedSecurity=true;";
        String connectionUrl1 = "jdbc:sqlserver://localhost:1433;databaseName=JavaSE;user=sa;password=123;";

        String SQL = "SELECT * FROM [Users] where usrLOGIN = ? AND usrPASSWORD = ? ; ";

        // Note: never do it this way !!!
        String SQL1 = "SELECT * FROM [Users] where [usrLOGIN] = \'%s\' AND [usrPASSWORD] = \'%s\' ; ";

        String SQL_INSERT = "INSERT INTO [Emails] () values () ;";
        try (Connection con = DriverManager.getConnection(connectionUrl1);
             Statement stmt = con.createStatement();
             PreparedStatement preparedStatement = con.prepareStatement(SQL)) {

//            ResultSet data =  stmt.executeQuery("select usrID as ID ,usrNAME as Name  from Users ; ");
//
//            if(data != null)
//            {
//                int cols = data.getMetaData().getColumnCount() ;
//                for (int i = 1; i <=  cols; i++) {
//
//                    System.out.print(data.getMetaData().getColumnName(i) + ( i != cols ? ", " : ""));
//                }
//                System.out.println();
//                while (data.next())
//                {
//                    for (int i = 1; i <=  cols; i++) {
//                        System.out.print(data.getString(i) + ( i != cols ? ", " : ""));
//                    }
//                    System.out.println();
//
//                }
//            }
            System.out.print("Login:");
            String login = scanner.nextLine().trim();

            System.out.print("Password:");
            String password = scanner.nextLine();

            // this is bad since it will allow sql injection such as entering (' or '1' = '1) for login and password
            // it will show all the data
            /**/ResultSet data =  stmt.executeQuery(String.format(SQL1,login,password));


//            if(data != null)
//            {
//                int cols = data.getMetaData().getColumnCount() ;
//                for (int i = 1; i <=  cols; i++) {
//
//                    System.out.print(data.getMetaData().getColumnName(i) + ( i != cols ? ", " : ""));
//                }
//                System.out.println();
//                while (data.next())
//                {
//                    for (int i = 1; i <=  cols; i++) {
//                        System.out.print(data.getString(i) + ( i != cols ? ", " : ""));
//                    }
//                    System.out.println();
//
//                }
//            }else{
//                System.err.println("No data");
//            }
//
//
            // TODO: Connect to any database you have at home
            // set the first ? to be login and the second ? to be password
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
//
//

            ResultSet rs = preparedStatement.executeQuery();

            // Iterate through the data in the result set and display it.
            int columns = rs.getMetaData().getColumnCount();
            if (rs.next()) {
                do {
                    for (int i = 1; i <= columns; i++) {
                        System.out.printf("%-25s | ", rs.getString(i));
                    }
                    System.out.println();
                } while (rs.next());


            } else {
                System.err.println("No data");
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}