package basics;

import java.util.ArrayList;

public class Variables {


    // Variable : is a symbolic name (an identifier) paired with a storage location (identified by a memory address)
    // which contains some known or unknown quantity of information referred to as a value.

    // Syntax: to define a variable
    //        [modifiers] [specifier] [specifier] <data_type> <variable_identifier> [=<expression>][, <variable_identifier> [=<expression>]]*;


    // Type of variables based it's location

    // variable can be defined in one of three places (locations)

    // 1) inside a a block (like a method)           (local variable)
    // 2) inside an object          (instance variable)
    // 3) inside a class           (static / class variable)

    // Note : both 2 and 3 are *written* inside the class but the class variables are static

    // In Java local variables must be defined before been used.


    int instanceVariable;              // instance variable

    static int classVariable;              // class variable

    public static void main(String[] args) {


        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add("s");

        double k;
        k = 2;


        // Example :
        int x = 10;                     // x is a variable
        Object o = new Object() ;        // o is a variable



        int a = Math.abs(15 + 2);

        int m = 2, n= 3, r = 15;
        x = 20;
        int m3  = 20;

        // Zalid variable identifier
        int c$d, c_d;

        // Invalid variable identifier
        //  c-d , 3d , int

        System.out.println(33);



        int localVariable;


        // Data types            c++ : bool    ,char ,    , short, int ,long  ,float , double
        // 1) Primitive Data Types   : boolean ,char ,byte, short, int ,long  ,float , double
        // 2) Reference/Object Data Types  : any other types



        int fa = 010;
        System.out.println("fa = " + fa);        //8

        boolean b = true;




        // integers : byte ,short , int , long

        byte by = 127 ;        // -128 to 127
        long l = 2147483648L;  // 9223372036854775807 ;

        float f = 10.54F;          // F
        double d = 10.54;

        int val = 0x1abcdef;
        System.out.println("val = " + val);

        System.out.println("-------------------------");
        val = 010 ;
        System.out.println("val = " + val);     // 2 , 10 , (8) , 12 , Error

        val = 0x10 ;
        System.out.println("val = " + val);     // 2 , 10 , 8 , (16) , Error

        System.out.println("-------------------------");
        // Characters

        char ch = 'A';
        System.out.println("ch = ." + ch);              // A
        ch = 65;
        System.out.println("ch = ." + ch);              // A
        ch = 0101;                                      // Any number started with 0 is an octal representation
        System.out.println("ch = ." + ch);              // A
        ch = 0x41;                                      // Any number started with 0x is an hexadecimal representation
        System.out.println("ch = ." + ch);
        ch = '\u0041';
        System.out.println("ch = ." + ch);



        // Type conventions :
        //  we can convert byte --> short--> int--> long --> float -->double


        // there are some risky cases :
        //      1) int--> float : for big integer value
        int value = 2147483644;
        float target = value;
        int back = (int) target;

        System.out.println("value  = " + value);
        System.out.println("target = " + target);
        System.out.println("back = " + back);


        System.out.println("----------------------");
        //      2) long --> float : for big long value
        long longValue = Long.MAX_VALUE - 5;
        target = longValue;
        long  Lback = (int) longValue;
        System.out.println("longValue  = " + longValue);
        System.out.println("target     = " + target);
        System.out.println("Lback = " + Lback);
        System.out.println("----------------------");
        
        //      3) long --> double : for big long value

        double dubTraget = longValue;
        Lback = (long) dubTraget;
        System.out.println("longValue  = " + longValue);
        System.out.println("dubTraget     = " + dubTraget);
        System.out.println("Lback = " + Lback);
        System.out.println("----------------------");
        // longValue = 10 ;

        int s = 1542;        // type casting

        System.out.println("s = " + s);
        System.out.println("Integer.toBinaryString(s) = " + Integer.toBinaryString(s));
        System.out.println("Integer.toHexString(s) = " + Integer.toHexString(s));
        System.out.println("Integer.toOctalString(s) = " + Integer.toOctalString(s));

        // Cast from string to integer
        s = Integer.valueOf("1254");
        System.out.println("s = " + s);

        s = Integer.valueOf("1254", 8);
        System.out.println("s = " + s);

        s = Integer.valueOf("1001", 2);
        System.out.println("s = " + s);

        s = Integer.valueOf("41A", 16);
        System.out.println("s = " + s);



    }


    //

}
