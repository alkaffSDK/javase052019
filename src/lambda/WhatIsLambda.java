package lambda;

public class WhatIsLambda {
    public static void main(String[] args) {


        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO :: Action
                System.out.println(Thread.currentThread().getName());
            }
        },"With No lambda");


        Runnable functionRef =   () -> {  System.out.println(Thread.currentThread().getName());} ;
        Thread t1 = new Thread(functionRef, "With function reference lambda");

        Thread t2 = new Thread(() -> {  System.out.println(Thread.currentThread().getName());},"With lambda");


        // if you have only one statement in the lambda body, you can rewrite it as following

         t2 = new Thread(() ->   System.out.println(Thread.currentThread().getName()),"With lambda");



        t1.start();
        t.start();
        t2.start();





    }


    interface  FunctionInterface {
        void run();
    }
}
