package lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

public class MethodReferenceDemo2 {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(
				new Person("Ahmed", "Alkaff", 34),
				new Person("Raghad", "Mando", 22),
				new Person("Omama", "Manaa", 26),
				new Person("Yazan", "AbuHamdodeh", 26),
				new Person("Imaseil", "Bibers", 23),
				new Person("Ahmed", "Dosogqei", 25),
				new Person("Yaser", "Meryan", 23)
				);

		System.out.println("Printing all persons");

		people.forEach(p -> System.out.println(p));
		people.forEach(System.out::println);

		//performConditionally(people,p->true,(v,k)-> System.out.println("hello "+k));
		performConditionally("Hello ",people,p->true,(s,p) -> MethodReferenceDemo2.PrintMe(s,p));

		performConditionally("مرحبا ",people,p->true,(MethodReferenceDemo2::PrintMe));

		foo("String",people.get(0),(s,p) -> MethodReferenceDemo2.PrintMe(s,p));

		foo("String",people.get(0),MethodReferenceDemo2::PrintMe);



		//performConditionally(people, p -> true, e -> System.out.println(e));
		//performConditionally(people,p->true,System.out::println);
        //performConditionally2(people, MethodReferenceDemo2::PrintMe, System.out::println);

	}

//	private static void performConditionally(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {
//		for (Person p : people) {
//			if (predicate.test(p)) { // true
//				consumer.accept(p);			// System.out.println(p)
//			}
//		}
//	}

	private  static void foo(String str, Person p , BiConsumer<String,Person> biConsumer)
	{
		biConsumer.accept(str,p);
	}
	private static void performConditionally(String str, List<Person> people, Predicate<Person> predicate, BiConsumer<String, Person> biConsumer) {
		for (Person p : people) {
			if(predicate.test(p))
				biConsumer.accept(str,p);
			//consumer.accept("hi " + p);
		}
	}
	public  static void PrintMe1(Person p , String s)
	{
		System.out.println(s+ " to "+p.getFirstName()+", "+ p.getLastName());
	}
	public  static void PrintMe(String s , Person p)
    {
		System.out.println(s+ " to "+p.getFirstName()+", "+ p.getLastName());
    }
}
