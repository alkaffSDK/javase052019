package sharedClasses;

public class Person {



    static  {
        System.out.println("Person static block" );
    }
    protected int Id;
    protected String Name ;
    private int Age;
    protected Gender gender ;

    public int getAge() {
        return Age;
    }

    public boolean setAge(int age) {
        if(age > 0 && age < 150) {
            Age = age;
            return  true;
        }
        return  false ;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void print()
    {
        System.out.println("Person");
    }
    public  void MethodInPerson()
    {

    }

    @Override
    public String toString() {
        return "Person{" +
                "Id=" + Id +
                ", Name='" + Name + '\'' +
                ", gender=" + gender +
                '}';
    }
}
