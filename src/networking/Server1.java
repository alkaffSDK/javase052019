package networking;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Server1 {
    public static void main(String[] args) {
        // 192.168.16.6
        int port = 9595;
        try (ServerSocket serverSocket = new ServerSocket(port)) {

            Map<String, Socket> clients = new HashMap<>();
            Socket client;
            String ClientName;
            System.out.println("Server is start listening....");
            while (true) {
                client = serverSocket.accept();
                ClientName = client.getInetAddress().getHostAddress() + ":" + client.getPort();
                clients.put(ClientName, client);
                System.out.println("A new client was connected from :" + ClientName);

                try (PrintWriter writer = new PrintWriter(client.getOutputStream())) {
                    writer.println("Hello, "+ClientName);
                    writer.flush();
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
