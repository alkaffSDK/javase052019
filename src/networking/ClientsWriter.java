package networking;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class ClientsWriter extends Thread {

    Socket mSockets ;
    BlockingDeque<String> messageBlockingDeque = new LinkedBlockingDeque<>(10);

    public ClientsWriter(Socket clients) {
        mSockets = clients ;
        start();
    }


    public boolean sendMessage(String msg)
    {
        try {
            messageBlockingDeque.addLast(msg);
            return  true ;
        }catch (Exception ex)
        {
            return  false ;
        }

    }

    @Override
    public void run() {

        try(PrintWriter writer = new PrintWriter(mSockets.getOutputStream()))
        {
            while (! mSockets.isClosed())
            {
               while(messageBlockingDeque.size() > 0)
               {
                   writer.println(messageBlockingDeque.poll());
               }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
