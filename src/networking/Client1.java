package networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Client1 {
    public static void main(String[] args) {

        String host = "localhost";      // 127.0.0.1
        int port = 9595;
        String line = "";
        BufferedReader reader = null ;
        try (Socket server = new Socket(host, port)){

             reader = new BufferedReader(new InputStreamReader(server.getInputStream()));

             while ((line = reader.readLine()) != null)
             {
                 System.out.println("Server :"+line);
             }

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(reader !=null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
