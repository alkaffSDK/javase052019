package networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientReader extends Thread {
    private Socket mSocket;
    private String mName;

    public ClientReader(String name, Socket client) {
        mSocket = client;
        mName = name;
        start();
    }


    @Override
    public void run() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(mSocket.getInputStream()))) {

            String line;
            while (!mSocket.isClosed()) {
                if (reader.ready() && (line = reader.readLine()) != null)
                    System.out.println(mName + ": " + line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
