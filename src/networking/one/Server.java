package networking.one;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Calendar;

public class Server {

    private static boolean isRunning = true;

    public static void main(String[] args) {

        String line = null;
        while (isRunning) {
            try (ServerSocket serverSocket = new ServerSocket(Constants.PORT)) {
                System.out.println("Server: Start listening @ " + serverSocket.getInetAddress().getHostAddress() + ":" + serverSocket.getLocalPort());

                Socket c = serverSocket.accept();
                System.out.println("Server: Client is connected from IP:" + c.getInetAddress() + ", Port:" + c.getPort() + " @" + Calendar.getInstance().getTime());
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()))) {
                    while (reader.ready()) {
                        System.out.println("Client :" + reader.readLine());
                    }
                }
                String clientName = "Client" + line.split(":")[1];
                try (PrintWriter writer = new PrintWriter(c.getOutputStream())) {
                    writer.println("Welcome " + clientName);
                    writer.flush();
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Server is closed");
    }
}
