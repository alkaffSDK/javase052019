package networking.one;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Locale;

public class Client {


    public static void main(String[] args) {

        String line = "";
        try (Socket client = new Socket(Constants.HOST, Constants.PORT)) {

            try (PrintWriter writer = new PrintWriter(client.getOutputStream());
                 BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()))) {
                System.out.println("Client" + client.getLocalPort() + " is started.");
                writer.println("I'm client from port:" + client.getLocalPort());

                while (reader.ready()) {
                    System.out.println("Server said :" + reader.readLine());
                    Thread.sleep(100);
                }
            }


        } catch (ConnectException ce) {
            System.err.println(String.format(Locale.getDefault(), "Server  @%s:%d is not connected.", Constants.HOST, Constants.PORT));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

}
