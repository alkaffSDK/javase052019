package networking.one;

public final class Constants {

    public static final String HOST = "127.0.0.1";   // or "localhost"
    public static final int  PORT = 10100;
}
