package networking;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class EchoClient {




    public static void main(String[] args) {

       try(Socket server = new Socket(Constants.SERVER_ADDRESS,Constants.SERVER_PORT);
           PrintWriter writer = new PrintWriter(server.getOutputStream()))
       {
           writer.println("Hello Server");

       } catch (UnknownHostException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }
        System.out.println("Client is closed");

    }
}
