package networking.SocketServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
    private static final String SERVER_IP = "127.0.0.1";
    private static final String PORT = "127.0.0.1";
    private static Socket server;
    static BufferedReader br;

    public static void main(String[] args) {
        // 127.0.0.1 , localhost
        try {
            server = new Socket(SERVER_IP, Server.PORT);
            InputStream mInputStream = server.getInputStream();
            br = new BufferedReader(new InputStreamReader(mInputStream));
            String line = null;
            while (null != (line = br.readLine())) {
                System.out.println("Server:" + line);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null)
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}
