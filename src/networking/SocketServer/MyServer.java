package networking.SocketServer;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class MyServer {


    public static final int PORT = 9911 ;
    static Socket mClient = null ;
    static BufferedReader reader = null  ;
    static PrintWriter printWriter = null ;
    public static void main(String[] args) {
        ServerSocket mServer = null;

        InputStream is = null ;
        OutputStream os = null ;


        Scanner sc = new Scanner(System.in);
        try {
            mServer = new ServerSocket(PORT);
            System.out.println("Server is started and waiting for connection..");
            mClient = mServer.accept();
            System.out.println("A client is connected "+mClient.getInetAddress()+":"+mClient.getPort());

           ReaderThread reader = new ReaderThread(mClient.getInputStream(),"Client");
           WriterThread writer = new WriterThread(mClient.getOutputStream());

           reader.start();
           writer.start();

            reader.join();
            writer.join();


            System.out.println("End connection");
            //os = mClient.getOutputStream() ;


        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                if(null != mServer )
                    mServer.close();
                if(null != reader)
                    reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
