package networking.SocketServer;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static final int PORT = 9932;

    static ServerSocket mServerSocket = null;
    static Socket Client;
    static PrintWriter pw;

    public static void main(String[] args) {
        try {
            mServerSocket = new ServerSocket(PORT);
            System.out.println("Waitting for a client to connect");
            Client = mServerSocket.accept();
            System.out.println("Client is connected with address :"
                    + Client.getInetAddress().getHostAddress());
            // pw = new PrintWriter(new BufferedWriter(new
            // OutputStreamWriter(Client.getOutputStream())));
            pw = new PrintWriter(Client.getOutputStream());
            pw.println("Welcome to our network.");
            pw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Finsh the connection");
            try {
                if (pw != null)
                    pw.close();
                if (Client != null)
                    Client.close();
                if (mServerSocket != null)
                    mServerSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
