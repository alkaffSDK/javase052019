package networking.SocketServer;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class MyClient {


    private static final String HOST = "127.0.0.1";

    public static void main(String[] args) {
        Socket mSocket = null ;
        Scanner sc = new Scanner(System.in);
        PrintWriter printWriter = null ;
        try {
            System.out.println("Client is started and will be connected to server");
            mSocket = new Socket(HOST,MyServer.PORT) ;

            ReaderThread reader = new ReaderThread(mSocket.getInputStream(),"Server");
            WriterThread writer = new WriterThread(mSocket.getOutputStream());

            reader.start();
            writer.start();

            reader.join();
            writer.join();


        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {

                try {
                    if(null != mSocket)
                    mSocket.close();
                    if(null != printWriter)
                        printWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}
